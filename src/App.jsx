import { useEffect, useState } from 'react';
import axios from 'axios';
import './App.css';
import ListCharacters from './components/ListCharacters';

function App() {
  const [character, setCharacter] = useState([])
  const API_URL = "https://rickandmortyapi.com/api/character";

  useEffect(() => {
    axios
      .get(API_URL)
      .then((response) => {
        setCharacter(response.data.results)
      })
  }, [])


  return (
    <>
        <ListCharacters characters={character}/>
    </>
  )
}

export default App

import { useState, useEffect } from "react";
import "./Character.css";

function Character(props) {
  const { name, image, status, species, gender, type } = props;
  const [alive, setAlive] = useState(status)

  function checkAlive(){
        if(alive === "Alive"){
            setAlive("Dead")
        } else if(alive === "Dead"){
            setAlive("Alive")
        } else {
            setAlive("unknown")
        }
    }
return (
    <li>
      <img src={image} alt={name} />
      <section className="character__details">
        <h3>{name}</h3>
        <p><strong>Espèce : </strong>{species}</p>
        <p><strong>Genre : </strong>{gender}</p>
        <p><strong>Type : </strong>{type ? type : "Pas de type"}</p>
      </section>
      <button className="character__status" onClick={checkAlive}>
        {alive === "Alive" ? "❤️" : alive === "Dead" ? "💀" : "❓"}
      </button>
    </li>
  );
}

export default Character;

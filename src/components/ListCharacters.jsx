import { useState, useEffect } from "react";
import Character from "./Character";
import "./ListCharacters.css";

function ListCharacters(props) {
  const { characters } = props;
  /*
    - (searchValue) On déclare un état pour la recherche 
    - (setSearchValue) On déclare une fonction pour modifier l'état de la recherche 
    - (useState("")) On déclare la valeur initiale de l'état à une châine de caractère vide
  */
  const [searchValue, setsearchValue] = useState("");

  /*
    - (charactersListFiltered) On déclare un état pour la liste filtrée de personnage 
    - (setcharactersListFiltered) On déclare une fonction pour modifier cette liste filtrée 
    - (useState(characters)) On déclare la valeur initiale de l'état comme la liste complète des personnages
  */
  const [charactersListFiltered, setcharactersListFiltered] =
    useState(characters);

  /*
    - (useEffect) permet de définir une action à exécuter
    - (setcharactersListFiltered) L'action à effectuer sera la mise à jour de la variable(charactersListFiltered) avec la valeur (characters)
    - ([characters]) l'effet de useEffect sera exécuté à chaque fois que la valeur de (characters) change
  */
  useEffect(() => {
    setcharactersListFiltered(characters);
  }, [characters]);

  /*
    - (handleSearchSubmit) On déclare une fonction fléchée qui prend pour argument un évènement 
    - (event.preventDefault()) On bloque le comportement par défaut
    - (filtered) On déclare une variable qui stocke le résultat suivant
    - (characters.filter) On filtre sur la liste des personnages, on retourne le nom de chaque personnage en ne prenant pas en compte la casse et en incluant l'état de la recherche
    - (setcharactersListFiltered) On met à jour la liste des personnages filtrés
    - (setsearchValue) On réinitialise l'état de la recherche à une chaîne caractère de vide
  */
  const handleSearchSubmit = (event) => {
    event.preventDefault();
    const filtered = characters.filter((character) => {
      return character.name.toLowerCase().includes(searchValue.toLowerCase());
    });
    setcharactersListFiltered(filtered);

    searchValue === null
      ? setcharactersListFiltered(characters)
      : setcharactersListFiltered(filtered);
  };

  return (
    <>
      <section className="search__box">
        <h1>Rick & Morty</h1>
        <form onChange={handleSearchSubmit}>
          {/* En soumettant le formulaire on effectue la fonction handleSearchSubmit()*/}
          <span className="material-symbols-outlined">search</span>
          <input
            type="text"
            name="search"
            placeholder="Rechercher"
            value={searchValue}
            onChange={
              (event) =>
                setsearchValue(
                  event.target.value
                ) /* Lorsque la valeur du input change l'état de la recherche prend la valeur de l'input */
            }
          />
        </form>
      </section>
      <ul>
        {charactersListFiltered.map((eachCharacter) => (
          <Character
            key={eachCharacter.id}
            name={eachCharacter.name}
            image={eachCharacter.image}
            status={eachCharacter.status}
            species={eachCharacter.species}
            gender={eachCharacter.gender}
            type={eachCharacter.type}
          />
        ))}
      </ul>
    </>
  );
}

export default ListCharacters;
